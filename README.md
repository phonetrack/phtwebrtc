# PhoneTrack WebRTC + SIP
Wrapper SIP + WebRTC

## Instalação

```sh
$ npm install pht-webrtc
```

Você pode importar o módulo:
```javascript
// ES6 modules
import { PhTWebRTC } from "pht-webrtc";

// commonjs
var PhTWebRTC = require("pht-webrtc");
```

Ou incluir o script diretamente na sua página:
```
<script type="text/javascript" src="https://phonetrack-static.s3.sa-east-1.amazonaws.com/pht-webrtc-1.0.10.min.js"></script>
```

### Exemplo de utilização

```javascript
var phtWebRTC = new PHTWebRTC();

// Informe as credenciais SIP:
phtWebRTC.setSipCredentials({
    uri: 'bob@example.onsip.com',
    wsServers: ['wss://sip-ws.example.com'],
    authorizationUser: '',
    password: ''
});

// Selecione o player
phtWebRTC.setRemotePlayer(document.getElementById("remote-player"));

// Faz a solicitação de conexão no servidor SIP
phtWebRTC.initialize();

phtWebRTC.on('onUserAgentRegistered', function() {
    // Realiza uma chamada telefônica
    phtWebRTC.call('554133333333');
});
```

## Documentação da API

### Métodos

#### setSipCredentials

#### call

Inicia uma chamada para um determinado destino.

`Parâmetros`

Nome | Tipo | Descrição
---- | ---- | ---------
target | String | Destino na ligação. Ex. 554199999999

`Retorno` Instância PhTWebRTC

`Emite` onSentInvite


#### end
Encerra a chamada ativa.

`Parâmetros` Nenhum.

`Emite` onHangup

`Retorno` Instância PhTWebRTC

#### toggleMute
Alterna o estado *mute* da chamada.

`Parâmetros` Nenhum.

`Retorno` Instância PhTWebRTC

`Emite` onSessionMuted

#### setSipCredentials
Define as credenciais de autenticação SIP.

`Parâmetros`

Nome | Tipo | Descrição
---- | ---- | ---------
credentials | Object | Objeto com as credenciais para conexão
credentials.url | String | SIP URI associado ao usuário. ex: bob@example.onsip.com
credentials.wsServers | Array | Array com os servidores websocket. ex: ["wss://sip-ws.example.com"]
credentials.authorizationUser | String | Usuário SIP
credentials.password | String | Senha de usuário SIP

`Retorno` Instância PhTWebRTC

#### setRemotePlayer
Define o player onde será executado o outro lado da chamada.
Definindo o player é possível ajustar o volume de saída de áudio e acompanhar a duração da chamada.

`Parâmetros`

Nome | Tipo | Descrição
---- | ---- | ---------
element | DOM Element Object | Elemento DOM <audio> ou <video>

`Retorno` Instância PhTWebRTC

#### setRecordBaseUrl
Define a url base das gravações

`Parâmetros`

Nome | Tipo | Descrição
---- | ---- | ---------
url | String | URL base das gravações. ex: http://sip3.phonetrack.com.br/records

`Retorno` Instância PhTWebRTC

#### setRTCConstraints
Define as contraints de WebRTC para *navigator.getUserMedia()*

`Parâmetros`

Nome | Tipo | Descrição
---- | ---- | ---------
constraints | Object | Objeto de definição das constraints
constraints.audio | Boolean | Indica se será usado o recurso de entrada de áudio (microfone)
constraints.video | Boolean/Object | Indica se será usado o recurso de entrada de vídeo (câmera), ou Objecto com as configurações de vídeo
constraints.video.width | Object | Objeto com as cnfigurações largura do vídeo
constraints.video.width.min | Number | Valor mínimo para largura de vídeo
constraints.video.width.max | Number | Valor máximo para largura de vídeo
constraints.video.height | Object | Objeto com as cnfigurações altura do vídeo
constraints.video.height.min | Number | Valor mínimo para altura de vídeo
constraints.video.height.max | Number | Valor máximo para altura de vídeo

`Retorno` Instância PhTWebRTC

[Saiba mais sobre WebRTC Constraints & statistics](https://webrtc.github.io/samples/src/content/peerconnection/constraints)

#### setMediaStream
Caso já tenha um MediaStream criado, pode ser usado nas chamadas.

`Parâmetros`

Nome | Tipo | Descrição
---- | ---- | ---------
mediaStream | MediaStream | Objeto do tipo MediaStream criado pelo *navigator.getUserMedia()*

`Retorno` Instância PhTWebRTC

[Saiba mais sobre MediaStream](https://developer.mozilla.org/pt-BR/docs/Web/API/MediaStream)

### Eventos

Exemplo de utilização de evento:
```javascript
phtWebRTC.on("onSentInvite", function(event) {
    alert("Ligação iniciada! Id da ligação: " + event.data.call.callId);
});
```

#### onSentInvite ####
Disparado quando um SIP invite é enviado.

`Parâmetros`

Nome | Tipo | Descrição
---- | ---- | ---------
data | Object | Objeto com dados do evento
data.call | PHTCall | Detalhes da chamada

#### onHangup
Disparado quando a ligação é encerrada.

`Parâmetros`

Nome | Tipo | Descrição
---- | ---- | ---------
data | Object | Objeto com dados do evento
data.call | PHTCall | Detalhes da chamada

#### onSessionProgress
Disparado cada vez que uma resposta provisória SIP (100-199) é recebida.

`Parâmetros`

Nome | Tipo | Descrição
---- | ---- | ---------
data | Object | Objeto com dados do evento
data.response | SIP.IncomingResponse | Resposta do servidor SIP

#### onSessionAccepted
Disparado cada vez que uma chamada é aceita pelo destino e uma resposta (200-299) é recebida.

`Parâmetros`

Nome | Tipo | Descrição
---- | ---- | ---------
response | SIP.IncomingResponse | Resposta do servidor SIP
status | String | Status de atendimento

#### onSessionBye
Disparado quando um SIP BYE é enviado ou recebido.

`Parâmetros`

Nome | Tipo | Descrição
---- | ---- | ---------
data | Object | Objeto com dados do evento
data.response | SIP.IncomingResponse | Resposta do servidor SIP

#### onSessionTerminated
Disparado quando uma sessão é destruída, seja antes ou depois de ter sido aceita.

`Parâmetros`

Nome | Tipo | Descrição
---- | ---- | ---------
data | Object | Objeto com dados do evento
data.response | SIP.IncomingResponse | Resposta do servidor SIP
data.cause | String | Causa do termino da chamada

`Emite` onHangup

#### onSessionRejected
Disparado a cada vez que uma mal-sucedida resposta é recebida (300-699).

`Parâmetros`

Nome | Tipo | Descrição
---- | ---- | ---------
data | Object | Objeto com dados do evento
data.response | SIP.IncomingResponse | Resposta do servidor SIP
data.cause | String | Causa do termino da chamada

`Emite` onSessionTerminated, onSessionFailed

#### onSessionFailed
Disparado quando uma requisição falha. Esse evento só será emitido por sessões que ainda não foram aceitas.

`Parâmetros`

Nome | Tipo | Descrição
---- | ---- | ---------
data | Object | Objeto com dados do evento
data.response | SIP.IncomingResponse | Resposta do servidor SIP
data.cause | String | Causa do termino da chamada

`Emite` onSessionTerminated

#### onSessionCancel
Disparada quando uma ligação é cancelada pelo cliente.

`Parâmetros` Nenhum

`Emite` onSessionTerminated

#### onSessionRefer
Disparado quando uma referência é recebida, e que o usuário gostaria de lidar com a transferência no nível da aplicação.

`Parâmetros`

Nome | Tipo | Descrição
---- | ---- | ---------
data | Object | Objeto com dados do evento
data.request | SIP.IncomingRequest | Requisição SIP REFER

#### onSessionReplaced
Disparado quando um INVITE com substituição causou essa sessão para terminar e ser substituído por uma nova sessão.

`Parâmetros`

Nome | Tipo | Descrição
---- | ---- | ---------
data | Object | Objeto com dados do evento
data.newSession | SIP.Session | Nova sessão

#### onSessionDtmf
Disparado quando é enviado ou receibo um DTMF.


`Parâmetros`

Nome | Tipo | Descrição
---- | ---- | ---------
data | Object | Objeto com dados do evento
data.request | SIP.IncomingRequest | Requisição SIP
data.dtmf | SIP.Session.DTMF | Instância DTMF

#### onSessionMuted
Disparado quando é acionado o estado *mute* do mediaHandler.

`Parâmetros`

Nome | Tipo | Descrição
---- | ---- | ---------
data | Object | Objeto com dados do evento
data.audio | Boolean | Indica se o stream de áudio está no estudo de *mute*
data.video | Boolean | Indica se o stream de vídeo está no estudo de *mute*

#### onSessionUnmuted
Disparado quando é desligado o estado *mute* do mediaHandler.

`Parâmetros`

Nome | Tipo | Descrição
---- | ---- | ---------
data | Object | Objeto com dados do evento
data.audio | Boolean | Indica se o stream de áudio está no estudo de *mute*
data.video | Boolean | Indica se o stream de vídeo está no estudo de *mute*

#### onUserAgentConnected
Disparado quando a conexão WebSocket é estabelecida.

`Parâmetros` Nenhum

#### onUserAgentDisconnected
Disparado quando a conexão WebSocket é desligada.

`Parâmetros` Nenhum

#### onUserAgentRegistered
Disparado quando o SIP.register é bem sucedido.

`Parâmetros` Nenhum

#### onUserAgentRegistrationFailed
Disparado quando o SIP.register falha

`Parâmetros`

Nome | Tipo | Descrição
---- | ---- | ---------
data | Object | Objeto com dados do evento
data.cause | String | Causa da falha
data.response | SIP.IncomingResponse | Resposta do servidor SIP

### Objetos

#### PhTCall
Objeto que representa uma ligação

`Propriedades`

Nome | Tipo | Valor padrão | Descrição
---- | ---- | ------------ | ---------
callId | String | null | Id da chamada
to | String | null | Destino da chamada
from | String | null | Origem da chamada
recordUrl | String | null | URL da gravação da chamada
status | String | null | Status de termino da chamada
startTime | Unix Timestamp | null | Timestamp de início da chamada
endTime | Unix Timestamp | null | Timestamp de fim da chamada
totalTime | Unix Timestamp | null | Timestamp do diferencial do fim e início da chamada

#### SIP.IncomingResponse
[Veja a especificação do objeto aqui.](http://sipjs.com/api/0.7.0/sipMessage)

#### SIP.IncomingRequest
[Veja a especificação do objeto aqui.](http://sipjs.com/api/0.7.0/sipMessage)

#### SIP.Session
[Veja a especificação do objeto aqui.](http://sipjs.com/api/0.7.0/session)

#### SIP.Session.DTMF
[Veja a especificação do objeto aqui.](http://sipjs.com/guides/send-dtmf)
