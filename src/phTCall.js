export class PhTCall {
    constructor() {
        this.callId     = null;
        this.to         = null;
        this.from       = null;
        this.recordUrl  = null;
        this.startTime  = null;
        this.status     = null;
        this.endTime    = null;
        this.totalTime  = null;
    }
}
