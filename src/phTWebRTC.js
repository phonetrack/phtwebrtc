/**
* PhoneTrack WebRTC + SIP wrapper
* @version 1.0
* @author Marcel Rodrigo <marcel.rodrigo@vinder.com.br>
* @update 2016-08-25
*/

import "webrtc-adapter";
import { UA } from "sip.js";
import Evee from "evee";
import { PhTCall } from "./phTCall";

export class PhTWebRTC extends Evee {

    constructor() {
        super();

        this._call              = null;
        this._userAgent         = null;
        this._sipSession        = null;
        this._sipCredentials    = null;
        this._remotePlayer      = null;
        this._recordPath        = "http://sip3.phonetrack.com.br/records";
        this._rtcConstraints    = { audio: true };
        this._muted             = false;
        this._stream            = null;
    }

    initialize() {
        if(!this._stream) {
            if(!!navigator.getUserMedia) {
                navigator.getUserMedia(this._rtcConstraints,
                    (stream) => {
                        this._stream = stream;
                    },
                    (error) => console.error(error));
                } else {
                    console.error("WebRTC not supported.");
                }
        }

        this._connectSIP();
    }

    ////////////////////////////////////////////////////////////////////////////
    // events
    ////////////////////////////////////////////////////////////////////////////

    /**
    * Fired each time a provisional (100-199) response is received.
    * @param {SIP.IncomingResponse} response - The received response
    * @emits {event} onSessionProgress
    * @see http://sipjs.com/api/0.7.0/session/#progress
    * @private
    * @return void
    */
    _onSessionProgress(response) {
        this.emit("onSessionProgress", {
            response: response,
        });
    }

    /**
    * Fired each time a successful final (200-299) response is received.
    * @param {SIP.IncomingResponse} response - The received response
    * @param {Number} code - The status code of the received response, between 200 and 299.
    * response code.
    * @return void
    * @see http://sipjs.com/api/0.7.0/session/#accepted
    */
    _onSessionAccepted(response, code) {
        this.emit("onSessionAccepted", {
            response: response,
            code: code
        });
    }

    /**
    * Fired when a BYE is sent or received. Note: A BYE request will also cause
    * a terminated event to be emitted.
    * @param {SIP.IncomingRequest} request - Instance of the received SIP BYE
    * request.
    * @emits {event} onSessionBye
    * @see http://sipjs.com/api/0.7.0/session/#bye
    * @return void
    */
    _onSessionBye(request) {
        this._call.status = request.reason_phrase;
        this._call.endTime = new Date().getTime();
        this._call.totalTime = new Date(this._call.endTime - this._call.startTime).getTime();

        this.emit("onSessionBye", {
            request: request
        });
    }

    /**
     * Fired when the session is destroyed, whether before or after it has been accepted.
     * @see http://sipjs.com/api/0.7.0/session/#terminated
     * @param {SIP.IncomingResponse} response - Instance of the received SIP response that caused the termination, if there was one.
     * @param {string} cause - One value of Failure and End Causes, if there was one.
     * @emits {event} onSessionTerminated
     * @return void
     */
    _onSessionTerminated(response, cause) {
        if(!!response && !this._call.status) {
            this._call.status = response.reason_phrase;
        }

        this._call.endTime = new Date().getTime();
        this._call.totalTime = new Date(this._call.endTime - this._call.startTime).getTime();

        if(!this._call.status) {
            this._call.status = cause ? cause : "Normal Clearing";
        }

        this.emit("onSessionTerminated", {
            response: response,
            cause: cause
        });

        this.emit("onHangup", {
            call: this._call
        });
    }

    /**
     * Fired each time an unsuccessful final (300-699) response is received.
     * Note: This will also emit a failed event, followed by a terminated event.
     * @param {SIP.IncomingResponse} response - The received response
     * @param {String} cause - The reason phrase associated with the SIP
     * response code.
     * @emits {event} onSessionRejected
     * @see http://sipjs.com/api/0.7.0/session/#rejected
     * @return void
     */
    _onSessionRejected(response, cause) {
        this.emit("onSessionRejected", {
            response: response,
            cause: cause
        });
    }

    /**
     * Fired when the request fails, whether due to an unsuccessful final
     * response or due to timeout, transport, or other error. This event will
     * only be emitted by Sessions which have not yet been accepted.
     * @param {SIP.IncomingResponse} response - The received response. On a
     * failure not due to a SIP message, this will be null.
     * @param {String} cause - The reason phrase associated with the SIP
     * response code, or one of Failure and End Causes.
     * @emits {event} onSessionFailed
     * @see http://sipjs.com/api/0.7.0/session/#failed
     * @return void
     */
    _onSessionFailed(response, cause) {
        this.emit("onSessionFailed", {
            response: response,
            cause: cause
        });
    }

    /**
     * Fired when the session was canceled by the client. Note that this will
     * not be immediately followed by a rejected, failed, or terminated event.
     * Depending on the timing of the cancel, it may trigger a rejection response
     * or the session may be accepted and immediately terminated. In those cases,
     * the rejected and failed or bye event will be emitted as expected, and the
     * terminated event will then follow.
     * @emits {event} onSessionCancel
     * @see http://sipjs.com/api/0.7.0/session/#cancel
     * @return void
     */
    _onSessionCancel() {
        this.emit("onSessionCancel");

        if(!this._call.startTime) {
            this._call.startTime = new Date().getTime();
            this._call.endTime = new Date().getTime();
            this._call.status = "Request Terminated";
            this._onSessionTerminated(null, "Request Terminated");
        }

    }

    /**
     * Fired when a REFER is received, and the user would like to handle the
     * transfer at the application level. To have SIP.js automatically follow
     * the refer, use the session.followRefer(callback) function.
     * @param {SIP.IncomingRequest} request - Instance of the received SIP REFER request.
     * @emits {event} onSessionRefer
     * @see http://sipjs.com/api/0.7.0/session/#refer
     * @return void
     */
    _onSessionRefer(request) {
        this.emit("onSessionRefer", {
            request: request
        });
    }

    /**
     * Fired when the request fails, whether due to an unsuccessful final
     * response or due to timeout, transport, or other error. This event will
     * only be emitted by Sessions which have not yet been accepted.
     * @param {SIP.Session} newSession - The new session replacing this one.
     * @emits {event} onSessionReplaced
     * @see http://sipjs.com/api/0.7.0/session/#replaced
     * @return void
     */
    _onSessionReplaced(newSession) {
        this._sipSession = newSession;

        this.emit("onSessionReplaced", {
            newSession: newSession
        });
    }

    /**
     * Fired for an incoming or outgoing DTMF.
     * @param {SIP.IncomingRequest} request - Instance of the received SIP INFO
     * request.
     * @param {SIP.Session.DTMF} dtmf - DTMF instance.
     * @emits {event} onSessionDtmf
     * @see http://sipjs.com/api/0.7.0/session/#dtmf
     * @return void
     */
    _onSessionDtmf(request, dtmf) {
        this.emit("onSessionDtmf", {
            request: request,
            dtmf: dtmf
        });
    }

    /**
     * Fired when the session’s mute function is called and the MediaHandler’s
     * mute function returns.
     * @param {Object} data - Contains which parts of the media stream were
     * muted
     * @emits {event} onSessionMuted
     * @see http://sipjs.com/api/0.7.0/session/#muted
     * @return void
     */
    _onSessionMuted(data) {
        this.emit("onSessionMuted", data);
    }

    /**
     * Fired when the session’s unmute function is called and the MediaHandler’s
     * unmute function returns.
     * @param {Object} data - Contains which parts of the media stream were
     * muted
     * @emits {event} onSessionUnmuted
     * @see http://sipjs.com/api/0.7.0/session/#unmuted
     * @return void
     */
    _onSessionUnmuted(data) {
        this.emit("onSessionUnmuted", {
            data: data
        });
    }

    /**
     * Fired when the WebSocket connection is established.
     * @emits {Event} onUserAgentConnected
     * @return void
     */
    _onUserAgentConnected() {
        this.emit("onUserAgentConnected");
    }

    /**
     * Fired when the WebSocket connection attempt (or automatic re-attempt)
     * fails.
     * @emits {Event} onUserAgentDisconnected
     * @return void
     */
    _onUserAgentDisconnected() {
        this.emit("onUserAgentDisconnected");
    }

    /**
     * Fired for a successful registration.
     * @emits {Event} onUserAgentRegistered
     * @return void
     */
    _onUserAgentRegistered() {
        this.emit("onUserAgentRegistered");
    }

    /**
     * Fired for a successful registration.
     * @emits {Event} onUserAgentRegistered
     * @param {String} cause - One value of Failure and End Causes
     * @param {String} response - The SIP message which caused this to be
     * emitted, if one exists
     * @return void
     */
    _onUserAgentRegistrationFailed(cause, response) {
        this.emit("onUserAgentRegistrationFailed", {
            cause: cause,
            response: response
        });
    }

    ////////////////////////////////////////////////////////////////////////////
    // methods
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Open the SIP session
     * Create a SIP session object and bind some events
     * @param {String} target - Destination of the call.
     * @emits onSentInvite
     * @return PhTWebRTC: this instance
     */
    call(target) {
        let options = {};
        options.media = {};
        options.media.render = {};
        options.media.constraints = this._rtcConstraints;
        options.media.stream = this._stream;
        options.media.render.remote = this._remotePlayer;

        this._sipSession = this._userAgent.invite(target, options);
        this._sipSession.on("progress", this._onSessionProgress.bind(this));
        this._sipSession.on("accepted", this._onSessionAccepted.bind(this));
        this._sipSession.on("bye", this._onSessionBye.bind(this));
        this._sipSession.on("terminated", this._onSessionTerminated.bind(this));
        this._sipSession.on("rejected", this._onSessionRejected.bind(this));
        this._sipSession.on("failed", this._onSessionFailed.bind(this));
        this._sipSession.on("cancel", this._onSessionCancel.bind(this));
        this._sipSession.on("refer", this._onSessionRefer.bind(this));
        this._sipSession.on("replaced", this._onSessionReplaced.bind(this));
        this._sipSession.on("dtmf", this._onSessionDtmf.bind(this));
        this._sipSession.on("muted", this._onSessionMuted.bind(this));
        this._sipSession.on("unmuted", this._onSessionUnmuted.bind(this));

        this._call = new PhTCall();
        this._call.callId = this._sipSession.request.call_id;
        this._call.callId = this._sipSession.request.call_id;
        this._call.to = this._sipSession.request.to.friendlyName;
        this._call.from = this._sipSession.request.from.friendlyName;
        this._call.recordUrl = `${this._recordPath}/${this._sipSession.request.call_id}.mp3`;
        this._call.startTime = new Date().getTime();

        this.emit("onSentInvite", {
            call: this._call
        });

        return this;
    }

    /**
     * Close the SIP session
     * @return PhTWebRTC: this instance
     */
    end() {
        this._sipSession.terminate();

        return this;
    }

    /**
     * Toggle session mute state
     * @return PhTWebRTC instance
     */
    toggleMute() {
        if(this._muted) {
            this._sipSession.unmute();
        } else {
            this._sipSession.mute();
        }

        this._muted = !this._muted;
        return this;
    }

    /**
     * Open connection to SIP server
     * Create a SIP.js UserAgent instance
     * @see http://sipjs.com/api/0.7.0/ua/
     * @private
     * @return void
     */
    _connectSIP() {
        this._userAgent = new UA(this._sipCredentials);

        // bind events
        this._userAgent.on("connected", this._onUserAgentConnected.bind(this));
        this._userAgent.on("disconnected", this._onUserAgentDisconnected.bind(this));
        this._userAgent.on("registered", this._onUserAgentRegistered.bind(this));
        this._userAgent.on("registrationFailed", this._onUserAgentRegistrationFailed.bind(this));

        this._userAgent.start();
    }

    /**
     * Set SIP server auth credentials
     * @param {Object} credentials
     * @return PhTWebRTC instance
     */
    setSipCredentials(credentials) {
        this._sipCredentials = credentials;
        return this;
    }

    /**
     * Set remote output player element
     * @param {DOMElement} element - audio/video DOM element
     * @return PhTWebRTC instance
     */
    setRemotePlayer(element) {
        this._remotePlayer = element;
        return this;
    }

    /**
     * Set the WebRTC constraints to getUserMedia()
     * @param {Object} constraints - constraints object
     * @see https://webrtc.github.io/samples/src/content/peerconnection/constraints/
     * @return PhTWebRTC instance
     */
    setRTCConstraints(constraints) {
        this._rtcConstraints = constraints;
        return this;
    }

    /**
     * Set MediaStream to transmit to the other end
     * @param {MediaStream} mediaStream - The MediaStream object
     * @return PhTWebRTC instance
     */
    setMediaStream(mediaStream) {
        this._stream = mediaStream;
        return this;
    }

    /**
     * Set Record base URL
     * @param {String} url: base url for the records
     * @return PhTWebRTC instance
     */
    setRecordBaseUrl(url) {
        this._recordPath = url;

        return this;
    }
}
