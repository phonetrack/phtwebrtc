var path = require('path'),
    webpack = require('webpack');

module.exports = {
    entry: {
        "dist/pht-webrtc.min": "./src/index.js"
    },
    output: {
        path: __dirname,
        filename: '[name].js',
        libraryTarget: 'umd',
        library: ["PHTWebRTC"]
    },
    module: {
        loaders: [{
            test: /(\.jsx|\.js)$/,
            loader: 'babel',
            exclude: /(node_modules)/
        }, {
            test: /\.json$/,
            loader: 'json'
        }]
    }
};
